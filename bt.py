#!/usr/bin/env python

import os, sys, argparse
from subprocess import call
from configfile import ConfigFile

cf = ConfigFile("mrrabbit", "bt")
targets = cf.getRoot()

def listTargets():
    print(*targets.keys())

def connectTarget(target):
    if not target in targets:
        print("unkwnon device")
        sys.exit(1)
    t = targets[target]
    dir_path = os.path.dirname(os.path.realpath(__file__))
    call(['expect', '-f', dir_path + '/bt.expect', t])
    call(['pactl', 'set-default-sink', 'bluez_sink.' + t.replace(':','_') + '.a2dp_sink'])


parser = argparse.ArgumentParser(description='Connect to Bluetooth sound sink')
parser.add_argument('-v', '--version', action='version', version='%(prog)s 0.0.1')
parser.add_argument('-l', '--list', dest='l', action='store_true', help='list targets')
parser.add_argument('target', metavar='target', nargs='?', help='connect to this target')

args = parser.parse_args()

if args.l:
    listTargets()
elif args.target:
    connectTarget(args.target)
else:
    parser.print_help()

# bt (bluetooth)

Connect a configured bluetooth device.

This script uses expect to talk with bluetoothctl.

##  configuration

The configuration contains the dict of known devices "name" : "MAC address".
It is written in ~/.config/mrrabbit/bt/config.json

~/.config/mrrabbit/bt/config.json example:

```json
{
    "my_headset": "00:11:22:33:44:55",
    "another_one" : "00:11:22:33:44:66"
}
```
## zsh completion

_bt provides zsh completion to list configured devices.


## install

- copy bt.py (or symlink it without the .py extension) to a folder in $PATH
- create the json file
- if using zsh, copy '_bt' to the zsh completion directory

## TODO

- simplify json file creation by listing known devices
- exit instead of hanging if bluetoothd in not started